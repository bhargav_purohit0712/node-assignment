const express = require('express')

const isAuth = require('../middleware/auth')

const route = express.Router()
const restController = require('../controllers/rest')
const { body } = require('express-validator')

//add blog data
route.post(
  '/addBlogData',
  [body('title').trim(), body('author').trim(), body('desc').trim()],
  isAuth,
  restController.addBlogData
)

// fatching cat
route.get('/fetchCatData', isAuth, restController.fetchCatData)

//find cat by id
route.get('/findCatByid/:id', isAuth, restController.findCatByid)

//add blog cat
route.post('/addBlogCatData', [body('name').trim()],isAuth, restController.addBlogCatData)

//delete cat
route.delete('/deleteCatData/:catName', isAuth, restController.deleteCatData)

//edit cat
route.get('/editCatData/:name/:newName', [body('name').trim()],isAuth, restController.editCatData)

//fetch blog
route.get('/fetchBlogData', restController.fetchBlogData)

//for admin
route.get('/afetchBlogData', isAuth, restController.fetchBlogData)

//delete blog
route.delete('/deleteBlogData/:bid', isAuth, restController.deleteBlogData)

//edit blog
route.put(
  '/editBlogData',
  [body('title').trim(), body('author').trim(), body('desc').trim()],
  isAuth,
  restController.editBlogData
)

//fetch single post
route.get('/fetchSingleData/:id', restController.fetchSingleData)

//fot admin
route.get('/afetchSingleData/:id', isAuth, restController.fetchSingleData)

//fetch data by title
route.get('/fetchByTitle/:title', restController.fetchDataByTitle)

//fetch resent blog title
route.get('/fetchRecent', restController.fetchRecent)

//login
route.post('/login', restController.loginUser)

module.exports = route
