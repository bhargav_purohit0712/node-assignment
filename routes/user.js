const express = require('express')
const route = express.Router()

const userController = require('../controllers/user')

route.get('/', userController.getHome)

route.get('/login', userController.getLogin)

route.get('/blogDetails/:slug/id=:id', userController.getDetailsPage)

//err
route.use('/', userController.error)

module.exports = route
