const express = require('express')

const route = express.Router()
const adminController = require('../controllers/admin')

//display list of blog page
route.get('/displayBlog', adminController.displayBlog)

//display add blog page
route.get('/displayAddBlog', adminController.displayAddBlog)

//display cat list page
route.get('/displayBlogCat', adminController.displayBlogCat)

//display edit cat page
route.get('/displayAddBlogCat/:name',adminController.displayCatEdit)

//display add cat page
route.get('/displayAddBlogCat',adminController.displayAddBlogCat)

//display edit blog page
route.get('/displayEditBlogPage/:id',adminController.displayEditBlog)

//logout
route.get('/logout',adminController.logout)

//err
route.use('/',adminController.error)

module.exports = route
