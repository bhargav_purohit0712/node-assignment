const cat = require('../models/category')

exports.displayBlogCat = (req, res, next) => {
  res.render('admin/cat')
}

exports.displayBlog = (req, res, next) => {
  res.render('admin/index')
}

exports.displayAddBlog = (req, res, next) => {
  res.render('admin/addBlog')
}

exports.displayAddBlogCat = (req, res, next) => {
  res.render('admin/addcat')
}

exports.displayCatEdit = (req, res, next) => {
  res.render('admin/editCat')
}
exports.displayEditBlog = (req, res, next) => {
  res.render('admin/editBlog')
}
exports.logout = (req, res, next) => {
  res.render('admin/logout')
}

exports.error = (req, res, next) => {
  res.render('blog/404')
}
