const blog = require('../models/blog')
const user = require('../models/user')
const cat = require('../models/category')

const jwt = require('jsonwebtoken')

const bcrypt = require('bcryptjs')

exports.addBlogCatData = (req, res, next) => {
  const cname = req.body.name
  cat
    .find({ name: cname })
    .then(rest => {
      if (!rest.length) {
        const newCat = cat({
          name: cname
        })
        return newCat
          .save()
          .then(rest => res.status(201).json({ msg: 'created' }))
      } else {
        console.log('exi')
        return res.status(201).json({ msg: 'already exist' })
      }
    })
    .catch(err => res.status(400).json({ error: err }))
}

exports.fetchCatData = (req, res, next) => {
  cat
    .find()
    .then(allCat => {
      return res.status(201).json(allCat)
    })
    .catch(err => res.status(400).json({ error: err }))
}

exports.addBlogData = (req, res, next) => {
  const imgUrl = req.file.path
  const title = req.body.title
  const author = req.body.author
  const catName = req.body.cat
  const desc = req.body.desc

  cat
    .findOne({ name: catName })
    .then(res => {
      const bsave = blog({
        title: title,
        img: imgUrl,
        author: author,
        des: desc,
        category: res
      })
      bsave.save()
    })
    .then(rest => {
      res.status(200).json({ msg: 'sus' })
    })
    .catch(e => res.status(400).json({ error: e }))
}

exports.deleteCatData = (req, res, next) => {
  const cname = req.params.catName
  cat
    .findOneAndDelete({ name: cname })
    .then(resp => res.status(201).json({ msg: 'done..' }))
    .catch(err => res.status(400).json({ error: err }))
}
exports.editCatData = (req, res, next) => {
  const oldname = req.params.name
  const newName = req.params.newName
  cat
    .findOne({ name: oldname.trim() })
    .then(resp => {
      resp.name = newName
      return resp.save()
    })
    .then(r => res.status(200).json({ msg: 'done..' }))
    .catch(err => res.status(400).json({ error: err }))
}

exports.fetchBlogData = (req, res, next) => {
  blog
    .find()
    .populate('category')
    .then(resp => {
      return res.status(200).json(resp)
    })
    .catch(err => res.status(400).json({ error: err }))
}

exports.deleteBlogData = (req, res, next) => {
  const id = req.params.bid

  blog
    .findByIdAndDelete(id)
    .then(resp => res.status(201).json({ msg: 'done..' }))
    .catch(err => res.status(400).json({ error: err }))
}

exports.editBlogData = async (req, res, next) => {
  const id = req.body.id
  const imgUrl = req.file.path
  const title = req.body.title
  const author = req.body.author
  const catName = req.body.cat
  const desc = req.body.desc
  try {
    const catData = await cat.findOne({ name: catName })
    const updateBlog = await blog.findById(id)
    updateBlog.title = title
    updateBlog.img = imgUrl
    updateBlog.author = author
    updateBlog.des = desc
    updateBlog.category = catData
    return updateBlog.save().then(re => res.status(201).json({ msg: 'done..' }))
  } catch (error) {
    console.log(error)
  }
}

exports.fetchSingleData = (req, res, next) => {
  const bid = req.params.id
  blog
    .findById(bid)
    .then(re => res.status(201).json(re))
    .catch(err => res.status(400).json({ error: err }))
}

exports.fetchDataByTitle = (req, res, next) => {
  const title = req.params.title
  blog
    .find({ title: title })
    .then(data => {
      if (!data.length) {
        return res.json({ msg: 'nodata' })
      } else {
        return res.status(201).json(data)
      }
    })
    .catch(err => res.status(400).json({ error: err }))
}
exports.fetchRecent = (req, res, nexr) => {
  blog
    .find()
    .sort({ createdAt: -1 })
    .select('title slug')
    .limit(5)
    .then(data => res.status(200).json(data))
    .catch(err => res.status(400).json({ error: err }))
}

exports.loginUser = (req, res, next) => {
  const email = req.body.email
  const pass = req.body.pass
  let loadedUser

  user
    .findOne({ email: email.trim() })
    .then(user => {
      if (!user) {
        return res.status(401).json({ msg: 'no user found' })
      } else {
        loadedUser = user
        return bcrypt.compare(pass, user.pass)
      }
    })
    .then(isEqal => {
      if (!isEqal) {
        return res.status(401).json({ msg: 'password is wrong..' })
      }
      const token = jwt.sign(
        {
          email: loadedUser.email,
          userId: loadedUser._id.toString()
        },
        'bhargavTestTest',
        { expiresIn: '1h' }
      )
      res.status(200).json({ token: token, userId: loadedUser._id })
    })
    .catch(e => res.status(400).json({ error: e }))
}
exports.findCatByid = (req, res, next) => {
  const id = req.params.id
  cat
    .findById(id)
    .then(data => res.status(200).json(data))
    .catch(e => res.status(400).json({ error: e }))
}
