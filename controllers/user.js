const blog = require('../models/blog')

exports.getHome = (req, res, next) => {
  res.render('blog/index')
}
exports.getLogin = (req, res, next) => {
  res.render('blog/login')
}

exports.getDetailsPage = (req, res, next) => {
  res.render('blog/blogDes')
}
exports.error = (req, res, next) => {
  res.render('blog/404')
}
