const jwt = require('jsonwebtoken')

module.exports = (req,res,next)=>{

    const token = req.get('Authorization').split(' ')[1];
    let decodeToken
    try{
        decodeToken = jwt.verify(token,'bhargavTestTest')

    }catch(e){
        return res.status(500).json({msg:'somthing wents wrong'})
    }
    if(!decodeToken){
        return res.status(401).json({msg:'ur not authorised'})
    }
    req.uId = decodeToken.userId;
    next()
}