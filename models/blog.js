const mongoose = require('mongoose')

const slug = require('mongoose-slug-generator')
const route = require('../routes/admin')
mongoose.plugin(slug)

const Schema = mongoose.Schema

const postSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    img: {
      type: String,
      required: true
    },
    des: {
      type: String,
      required: true
    },
    slug: {
      type: String,
      slug: 'title'
    },
    author: {
      type: Object,
      required: true
    },
    category: {
      type: Schema.Types.ObjectId,
      ref:'Category'
    
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('Post',postSchema)