const express = require('express')
const mongoose = require('mongoose')

const path = require('path')

//for url data
const bodyParser = require('body-parser')

//for file
const multer = require('multer')

// for pass 
//const bcrypt = require('bcryptjs')

//const userM = require('./models/user')

const app = express()

//for file stroge
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'image')
  },
  filename: (req, file, cb) => {
    cb(
      null,
      new Date().toISOString().replace(/:/g, '-') + '-' + file.originalname
    )
  }
})

//for view ejs
app.set('view engine', 'ejs')
app.set('views', 'views')

//for routs
const admin = require('./routes/admin')
const user = require('./routes/user')
const rest = require('./routes/rest')

//for file filtering
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/JPG' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true)
  } else {
    cb(null, false)
  }
}

//for simple data
app.use(bodyParser.urlencoded({ extended: false }))

//for file upload and download
app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single('image')
)

//for css and bootstarp lib
app.use(express.static(path.join(__dirname, 'public')))

//for json parsing
app.use(bodyParser.json()) // application/json

//for CORS
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  )
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
  next()
})

//set path for featching img
app.use('/image', express.static('image'))

//creating default user
// app.use('/createU',(req,res,next)=>{

//   bcrypt.hash('Test123',12).then(
//     haspw => {
//       const user = userM({
//         email:"test@test.com",
//         pass:haspw,
//         name:'Test'
//       })
//       return user.save()
//     }
//   ).then(
//     r=> res.json({msg:'done...'})
//   )
//   .catch(
//     err=> console.log(err)
//   )
//})

//for restapi data
app.use('/rest', rest)

//for admin routs
app.use('/admin', admin)

//for user routs
app.use('/', user)

mongoose
  .connect(
    'mongodb+srv://bhargav:Bunny12345@cluster0.xyg3r.mongodb.net/blog?retryWrites=true&w=majority',
    { useUnifiedTopology: true, useNewUrlParser: true }
  )
  .then(res => {
    console.log('connected')
    app.listen(3000)
  })
  .catch(err => console.log(err))
